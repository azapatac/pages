﻿namespace Pages.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {
        public DetailPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(object sender, System.EventArgs e)
        {
            var mainPage = (MasterDetailPage)App.Current.MainPage;

            mainPage.Detail.Navigation.PushAsync(new ContentPage
            {
                Content = new WebView
                {                    
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Source = "https://net-university.ninja/",
                    VerticalOptions = LayoutOptions.FillAndExpand,
                }
            });
        }
    }
}