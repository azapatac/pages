﻿using System;

namespace Pages.Views
{
    public class MenuItemPage
    {
        public MenuItemPage()
        {
            TargetType = typeof(DetailPage);
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}