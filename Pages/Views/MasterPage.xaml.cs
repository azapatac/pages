﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pages.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public ListView ListView;

        public MasterPage()
        {
            InitializeComponent();

            BindingContext = new MasterPageViewModel();
            ListView = MenuItemsListView;
        }

        class MasterPageViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MenuItemPage> MenuItems { get; set; }

            public MasterPageViewModel()
            {
                MenuItems = new ObservableCollection<MenuItemPage>(new[]
                {
                    new MenuItemPage { Id = 0, Title = "DetailPage" },
                    new MenuItemPage { Id = 4, Title = "TabbedPage", TargetType = typeof(MyTabbedPage) },
                    new MenuItemPage { Id = 4, Title = "CarouselPage", TargetType = typeof(MyCarouselPage) },
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}
