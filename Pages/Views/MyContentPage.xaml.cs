﻿
using Xamarin.Forms;

namespace Pages.Views
{
    public partial class MyContentPage : ContentPage
    {
        public MyContentPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(object sender, System.EventArgs e)
        {
            App.Current.MainPage = new MyMasterDetailPage();
        }
    }
}
