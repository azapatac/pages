﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pages.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyTabbedPage : TabbedPage
    {
        public MyTabbedPage()
        {
            InitializeComponent();
        }
    }
}
