﻿using Pages.Views;
using Xamarin.Forms;

namespace Pages
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new MyContentPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}